# Projects

Here's a list of the projects I have done and I am proud of and what's
coming (eventually) in the future. Most of my projects are either
written in C or Python because these are my favourite programming
languages. Each project have a small description of what it is and
my opinion about the project (Especially for older projects where I
give some kind of retrospective about them).

## Best projects

This is a list of the best projects I've done and that deserves to be
on top of this list.

### lightrcg

lightrcg is a Raycasting game engine I've been working on for a
bit. It has a few features but lacks documentation and some features I
want to work on. It uses the DDA algorithm which turns out to actually
be a problem to properly implement doors (which still haven't been
implemented yet). I've seen some implementations doing some hacks by
actually rendering doors as a sprite. It has been a really fun and
challenging project, I really liked working on it and learned a lot of
stuff from it.

 * [Lode's Raycasting Tutorial](https://lodev.org/cgtutor/raycasting.html)
 * [Codeberg](https://codeberg.org/matthilde/lightrcg)

### VICERA

The VICERA is a fantasy console I made about 2 years ago or
something. This was my first ever big C project and I am still really
proud of what I've done considering my knowledge back then and
definitely deserves to be in the best projects list. I have been
working on a new CPU for the fantasy console but then I kind of gave
up.

 * [Codeberg](https://codeberg.org/vicera)

### PyClassic

pyclassic is a library that implements the Minecraft Classic protocol
along with a few Classic Procotol Extension packets. I've been working
on it mostly alone even though someone helped me in developing it. The
documentation is not very clear and I am not sure when I'll make the
docs cleaner. All I have done is documenting the functions.

 * [GitHub](https://github.com/pyclassic/pyclassic)
 * [Documentation](https://pyclassic-docs.matthil.de)
 
## Side projects

### BFVM

BFVM is a simple fast brainfuck interpreter. It started as a bytecode
virtual machine (hence BFVM) but turned out to become a JIT
interpreter as I realised that the compiled bytecode could easily be
turned into x86 machine code.
Even though it's one of my best projects, I still consider it as a
side project considering the size of it. It may move in the list above
if I do what I planned a while ago (which is refactoring
everything, add optimization for common brainfuck patterns and
ports to various operating systems, CPU architectures and eventually
also implement an actual compile feature where it can compile to an
executable binary).

It can run mandelbrot.bf (included in the repo) in about 3 seconds on
my Core i5 in a single thread.

 * [Codeberg](https://codeberg.org/matthilde/bfvm)
 
### Reverse-engineered UM8

One day I've been given a reverse engineering challenge. This was a
small Win32 interpreter of a mysterious programming language. It took
me a weekend to go through it, document the interpreter, write an
hello world in it and port it to standard C99.

 * [Codeberg](https://codeberg.org/matthilde/um8-writeup)
 * [Esolangs Wiki](https://esolangs.org/wiki/UM8)

### huffman.c

This is an implementation of the Huffman Coding written in C, which is
a lossless compression algorithm mostly known for being used in
compression formats such as GZip (which uses the DEFLATE algorithm, a
mix of Huffman and LZ77). I implemented it to learn about this
algorithm but also to get out of my comfort zone about linked lists
and node trees (for some reason I really didn't liked linked lists).

The compression ratio is kind of lame but hey, it's not mean to be
used as actual compression software.

 * [Codeberg](https://codeberg.org/matthilde/huffman.c)

### shittpd

Small web server written in Bash which supports CGI (somehow). I don't
have much to say about it honestly, it's just a webserver that is
probably not even safe to use. I don't even use it when I need a local
web server.

 * [Codeberg](https://codeberg.org/matthilde/shittpd)

### Mbasic

A few years ago, I wrote a BASIC interpreter. It was kind of wacky but
it worked. However it was really simple despite the very
uhm... unoptimized implementation I have done. So I came back to it
and wondered if I could do an implementation of it but in as less
lines as I can. This was my attempt at making a small BASIC dialect in
around 200 lines of C.

 * [Codeberg](https://codeberg.org/matthilde/mbasic)
 
### Bogus

This is a stack-based esoteric programming language where the only
thing you can push is a random integer. It was quite fun to program
stuff in it. The hard part is to get the exact number you want ;). It
is apparently turing complete but the one who started writing a proof
just gave up. (clue: boolean logic is easy to do in this lang. rule 110)

 * [Codeberg](https://codeberg.org/matthilde/bogus)

### Falsys

FALSE is an esoteric programming language initially written in 68k
assembly for the Amiga microcomputer by Wouter Van Oortmerssen. When I
looked at the programming reference and saw the \` (which can execute
arbitrary 68k code, which is not possible if you are running on
another computer architecture). I was thinking "why not replacing this
with a way to do Linux syscalls". And this is why I wrote this. It
also has a weird (memory-leaky because I was lazy) lambda
implementation that allows it to also be used as a buffer the
programmer can modify. This allowed me to implement a webserver in
FALSE.

 * [Codeberg](https://codeberg.org/matthilde/falsys)
 
## The Trashcan

Projects I do not consider unreleased or dead, but mostly just "trash"
because it came from a joke that went too far. Sometimes my motivation
in programming stuff just comes from jokes between friends.

### sdjkjnkjskjckdcdcn

This is an AI using the Markov Chain that I wrote to imitate the
keysmashing of someone. Just keysmash for a long while in a file,
train the AI with it and it should be able to somewhat reproduce the
way you keysmash. Just don't break your keyboard when keysmashing.

 * [Codeberg](https://codeberg.org/matthilde/sdjkjnkjskjckdcdcn)
 
### Progress Quest as a Service (PQaaS)

This could be in my list of side projects but it just started off as a
joke about Progress Quest. For some reason one day I remembered about
this funky game called Progress Quest. I booted the game, looked at it
and thought "what if I run it in a server". Unfortunately, there only
had a win32 build and guess what: I was too lazy to actually
reimplement the thing myself (even thought the source is available and
I could just port it to be "server-friendly"). This led me to set up
Visual Studio on a Windows virtual machine and develop a program in C
(it's actually C++ but literally uses none of the C++ features) that
would spawn Progress Quest as a "slave" process and extract the GUI
information from the running game every time it gets updated, it would
then log it to stdout.

All I then had to do is making a web application that takes this log
feed and put it on a webpage. The website sometimes runs but not all
the time. So don't be surprised if there is nothing on the PQaaS
website.

I had to install Windows Server on a machine to let it run (thanks to
a friend for giving me a free key so I don't have to torrent).

If you are too lazy to install Visual Studio and compile this. There
is an available binary in the Codeberg repository.
**Make sure you run Progress Quest 6.2, latest versions does not work.**

 * [Website (sometimes down)](https://pqaas.matthil.de)
 * [Codeberg](https://codeberg.org/matthilde/pqaas)
 
### mulisp

"Hey, what if we wrote a lisp that fits in a Python lambda?"  
"lol sure"

 * [Codeberg](https://codeberg.org/matthilde/mulisp-min)

## Unreleased/Dead projects

This is a list of the either dead or unreleased projects I've been
working on. Some unreleased projects are unreleased because I am still
working on it.

### VIC-SM8

The VIC-SM8 is a fantasy CPU that was supposed to replace the wacky
VIC-8P fantasy CPU used in the VICERA. It had notable differences:
It could handle 16-bit integers as good as 8-bit integers, supported
interrupt vectors and used I/O virtual ports that could be mapped to
an external C function (instead of the memory-mapped I/O of the
VIC-8P). It was then also designed to be embedded in other
applications than the VICERA.

It currently has a somewhat working emulator with a working assembler
that has a syntax inspired by FORTH and the early versions of the uxn
assembler (the CPU itself was also quite inspired by uxn).

Maybe one day I'll work on it again.

 * [Codeberg](https://codeberg.org/vicera/vic-sm8)

### RAYTRACE.C (unreleased, WIP)

This is a project I am currently working on at the time I am writing
this. This is a port of Ken Silverman's GROUFST2.BAS QBasic raytracing
program written in C for DOS32. Currently it is just a port of said
program but I am considering adding my own features or even improving
the raytracer itself because it has its limitations.

This project is written for MS-DOS because I also wanted to learn more
about DOS programming.

### Chyno (unreleased)

This is an experimental LISP dialect I have been working on with my
girlfriend. We planned a lot of weird features because this
programming language is designed to be cursed.

### unix.lgbt (kinda dead)

I and a group of friends have been working on the pubnix (a public
access UNIX machine where people can make stuff and interact with each
other using social interaction services we've either implemented or
hosted). It's kind of a dead thing now, we do not really take
registrations anymore and I am no longer really maintaining it. I am
however really proud of it

 * [unix.lgbt](https://unix.lgbt)

### kibtyland (dead)

It was a 4chan-style image board written in TypeScript with the Deno
runtime. It has been written specifically to test this programming
language as someone begged me to try it. I planned to continue it or
port it to another language because it wasn't too much of a bad idea
then I got lazy. It used to run as a beta-testing ground in some
server. It's off now.

 * [Codeberg](https://codeberg.org/matthilde/kibtyland)

### MCUPL (just broken and not worth continuing)

This was a short attempt at making a functional programming langauge
that kind of looked like a weird mix of Erlang and ML-family
langauges. I was stupid enough to think that making a language relying
on recursion in Python would be a great idea.

 * [Codeberg](https://codeberg.org/matthilde/mcupl)
 
 
### Zala (i gave up)

Zala was my attempt at OSdev and for some reason I just stopped
developing it. I don't even remember what I've done to make QEMU go
bonkers and cause a triple fault (probably something related to the
IRQ).
