# Blog

Welcome to my blog! Pick a post.

 - [Rolling my own 6502 and Apple I Emulator for learning](/b/emu6502.html)
 - [Shortwave Listening, my new hobby](/b/shortwave.html)
 - [Starting a research lab](/b/research.html)
 - [Creating unix.lgbt](/b/unix_lgbt.html)
 - [Fleeing tech giants and mainstream internet/software...](/b/bye_giants.html)
 - [Creating a wiki](/b/wiki.html)
 - [My server Version 5](/b/version_5.html)
 - [Web Protocol Idea](/b/web_protocol.html)
 - [Random Blog #1: Kernel and Ideas](/b/random_1.html)
 - [End of VICERA :pensive:](b/end_of_vicera.html)
 - [New Project: the VICERA Fantasy Computer](b/vicera_part1.html)
 - [Switching from CloudFlare to a VPS](/b/bye_cloudflare.html)
 - [Roguelike in 350 lines of C](/b/c_roguelike.html)
