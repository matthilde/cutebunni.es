# Projects

This is the projects I am either working on or I have finished making.
Some are object to be improved at some point.

## Best projects

Some picks of the best projects I have done so far!

### Chyno
**Work in progress!** An experimental LISP-family functional programming
language that supports macro and has Bytecode abstraction.

**The project developement is currently on hold.**

### kibtyland
**soon!**. 4chan-style image board written in TypeScript, a friend begged
me to try the Deno runtime and TypeScript so I decided to make a project
on it.

### PyClassic
pyclassic is a Python implementation of the Minecraft Classic protocol
used to play Classic multiplayer. It has been used to write bots in
this game. It used to be private but now I made it public.

Documentation is not very clear yet. I need to work on it.

 * [GitHub repository](https://github.com/pyclassic/pyclassic)

### unix.lgbt
LGBTQ-Friendly public access UNIX server to create stuff together.

 * [unix.lgbt](https://unix.lgbt) 
 * [unix.lgbt Git server](https://git.unix.lgbt)

### VICERA
A fantasy console inspired of the Nintendo Gameboy.

 * [Git Repository](https://github.com/vicera)

## Side projects

Some side projects that I don't think deserve as much attention as my
other projects.

### .#@%
A very basic roguelike written in 350 lines of C.  

 * [Git Repository](https://git.matthil.de/matthilde/roguelike)

### BFVM
Optimizing Brainfuck Interpreter that uses Bytecode abstraction to
either run code faster or compile it easily into x86_64 code to make
a Just-In-Time compiler. (runs Mandelbrot set in 2 seconds!)

 * [Git repository](https://git.unix.lgbt/matthilde/bfvm)

### UM8 Reverse Engineering Writeup
This is a writeup of the UM8 virtual machine reverse-engineering
contest. This was a fun project to do! (and my first reverse-engineered
program). Contains a few sample programs and a C99 port of the original
Win32 interpreter.

 * [Git repository](https://git.unix.lgbt/matthilde/um8-writeup)
 * [Esolangs Wiki page](https://esolangs.org/wiki/UM8)

### MCUPL
Functional programming that I was too lazy to document. Figure it out
yourself by reading this one giant chunk of Python code I wrote to get
it working.

 * [Git repository](https://git.unix.lgbt/matthilde/mcupl)

### shittpd
Webserver with CGI support written in Bash.

 * [Git repository](https://git.unix.lgbt/matthilde/shittpd) 
 * [Website](https://unix.lgbt/~microwave/shittpd)

### Mbasic
I challenged myself to write a small BASIC dialect in as less lines as
possible. This is a dialect that has been written in around 200 lines
of C!

 * [Git repository](https://git.unix.lgbt/matthilde/mbasic)

### Bogus
Stack-based esoteric programming language that can only push random
integers.

 * [Git repository](https://git.unix.lgbt/matthilde/bogus)

### Falsys
Linux-only FALSE dialect that implements a command to support syscalls.
This allowed me to write a (completely dangerous and buggy) webserver
using it.

 * [Git repository](https://git.unix.lgbt/matthilde/falsys)

## Projects made ironically

Because I was bored.

### sdjkjnkjskjckdcdcn
AI that uses Markov Chain to immitate keyboard smashing of people.
Samples included.

[Git repository](https://git.unix.lgbt/matthilde/sdjkjnkjskjckdcdcn)

### Progress Quest As A Service
Turns a Progress Quest session into a webpage using a special programs
that uses the Win32 API to read information contained in the game
session so I can run it in my server. This project was from a joke with
a friend and also a fun way to discover the Win32 API.

 * [Watch the session live here](https://pqaas.matthil.de)
 * [Git Repository](https://git.unix.lgbt/matthilde/pqaas)
 * [Download (pqaas.exe)](https://git.unix.lgbt/matthilde/pqaas/src/branch/master/Release/pqaas.exe)

## Old, abandonned projects

### Zala
My attempt at writing an operating system kernel.

 * [Git repository](https://git.unix.lgbt/matthilde/zala)

### sea
A POSIX Shell script package manager for shell scripts. Since the project
became quite uninterresting, we abandonned it. I keep it there because I am
kinda proud of it.

 * [Git Repository](https://github.com/matthilde/sea)
