# Changelog

## v6 - CSS change and website rewrite

## v5.2 - CSS change

Modified the whole CSS because I got bored of the previous layout.

## v5.1 - Minor changes

Rewrote a few things and got rid of funbox's public access for now. Adding a
wiki to put in personal notes and small things that does not deserve a whole
site or a whole blog.

## v5 - Fresh reinstall

I have reinstalled everything on my server because the file structure was a big
mess, making it a bit inconvenient to do development or deploy other apps. So
I have decided to reinstall everything. At the same time, I have did a few minor
changes to the website to make it look better. Such as CSS fixes and a fresh new
epic gamer dropdown menu.

**Funbox** will be up at some point. Which is a small Raspberry Pi server I will
use to experiment and make applications just for fun. I will give it a subdomain
whenever I want to share the experiments publicly.

I may also host honk at some point or completely migrate from gitea to something
else. Not planned yet but I am thinking about it.

## v4.1 - Small changes

Rewrote all my site's markdown and got rid of solarr.h3liu.ml. 

## v4 - Back to minimalism

This is the current layout of my site. I have stopped overthinking about a good
web design and decided to make something that "just works". No complex CSS or
whatever, just the required to make a good looking site.

And the cool thing about this site, is that it's Mothra-compatible (poggers).  
Also Bludit and Flask apps are shit so now I use a static blog.

I have also updated the way I edit my site, which now works through a Git repo.

[Solarr's site](https://solarr.h3liu.ml) was released at the same time. Which
was the portfolio of someone that I forgot about now. Trying accessing to this
site will just give you a 502 Bad Gateway.

## v3 - Crappy layout attempt

Basically I have tried to make a website that looks like my computer rice. But
turns out it was complete shit so I have changed it shortly after it's release.

Here is the original `changes.html` of the v3 (This is literally the only
version where I have found the changelog file. Others has been deleted.)

    SITES UPDATES
    
    BLOG COMING SOON!
    
    I finally found an interest to make my blog. I will talk about the projects
    I make, random stuff that goes through my mind and kinda writeups of what I
    learn. Unlike the previous version of my blog. This blog will have a better
    layout. Still gonna try to keep it simple and minimalistic but will be
    better than the previous one.
    
    3RD WEBSITE REDESIGN
    
    The old one was kinda borked and I got bored of it. So I made a new one.
    And the epic thin about this one is that it looks like my desktop!
    
    PORTFOLIO
    
    Basically I show up the best projects I have done or I am making.

## v2 - New, fresh layout

New, simple layout. Blog has been deleted a bit after the new site. Webserver
has been moved to a Dell OptiPlex shortly before the release of the v3.

## v1 - First site layout

First version of the site.  
The previous domain was heatingappliance.tk and the webserver were hosted on a
Raspberry Pi. The thing that has never changed since is NGINX, really poggers
webserver.
