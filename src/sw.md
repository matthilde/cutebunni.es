# Radio-communication listening

Originally dedicated to shortwave radio, this place is dedicated to my
radio hobby. I listen to various band, ranging from the 80m ham band to
the 70cm ham band, with everything in-between.

I either go outside with my yagi antenna to look for satellites to
listen, to decode APT from NOAA or APRS from the ISS, or I listen to
the local repeaters. But the most interesting is listening to shortwave
radio on a weekend evening! You don't know what you can find outside
allocated bands! Could be a pirate SSTV broadcast, or a music AM
broadcast!

This place will also be dedicated to ham radio once I'll get my license
and be able to transmit in the air.

You can view the logs [here](/shortwave/)
