# Leaving Cloudflare.

_Sun 09 Aug 2020 01:19:17 AM_

Since I am self-hosting my webserver. I need an additional protection from DDoS or any other attacks I am not able to cover. So I used Cloudflare to protect my webserver but it was soon not enough. I needed to open more ports and Cloudflare won't proxy more than port 80.

### Why leaving Cloudflare?

Because Cloudflare wasn't enough. Cloudflare only lets you proxy websockets and HTTP traffic securely, else it's **gray-clouded**. And I need to open more than 80 (25565 for my Minecraft server). And I don't want to spend 20 bucks a month for Spectrum, so I took an alternative solution.

I decided to buy a VPS and manually proxy all my stuff and add the HTTPS encrypted connection myself. I decided to pick the cheapest [OVH VPS](https://ovh.com) to do this. 2 gigs of RAM, 1 vCore and a 40 GB NVMe which is way more than enough to run the applications I need.

### Switching

switching from Cloudflare to the VPS has been done in arround a span of 2-3 hours without much issues except that the Freenom and OVH dashboard is really slow.

For my VPS, I decided to pick Fedora since I am already familiar with it (using it for my main server). And because it's a good distro for server-side in my opinion.

I still use the same domain name so I got rid of the Cloudflare nameservers and put back the Freenom ones. The Freenom dashboard is kinda bad but it's not THAT bad, it's usable.

It's now time to configure everything in the VPS. Here is the software I picked for proxying my services:
	- Nginx to proxy my webserver
	- BungeeCord to proxy my Minecraft server

I will maybe need more services to proxy but that's what I need for now. Time to configure Nginx.

### Setting up nginx

The nginx configuration was pretty straight forward. I just needed to set up the domains and `proxy_pass` to my server IP address. Nothing more

	server {
	    server_name h3liu.ml git.h3liu.ml solarr.h3liu.ml, etc..;
	    listen 80;

	    location / {
	        proxy_pass http://my_ip/;
	    }
	}

But the connection is unsecure: I need an HTTPS certificate. To get it, I used Letsencrypt which is a free certificate provider. It provides a tool named `certbot` to get a certificate. I assigned my domain names to the certificate and I got one.

Since I reverse proxy on my own VPS, I also decided to add my own "502 Bad Gateway" page in case my server is offline. You can check it out [here](/500.html).

### Setting up BungeeCord

BungeeCord acts as a proxy for Minecraft servers just like nginx which allows me to add a security layer between my server and the players and actually let me invite people without having to give away my home IP address.

To configure it, I just had to put my server IP address in the configuration and I was good to go.

### New firewall rules

I just added a few firewall rules on my server so it only lets the incoming traffic of the webserver and the Minecraft server from the VPS IP address.

### Conclusion

i use a vps and its better than cloudflare.
