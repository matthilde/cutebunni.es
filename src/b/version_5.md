# Website v5

_Sat Dec 12 09:52:44 AM CET 2020_

The v4 of the website has been truly the best version of my site so far. But I
never get enough so this is why I am doing to make a V5. This version will not
provide any (major) changes regarding the website but most likely internal
stuff and new features...

The v5 will also be designed to be prepared for a potential incoming domain
name change because I am not gonna keep this free domain forever.

First off, I am getting rid of Docker. Docker is messed up and there are better
alternatives that can even run without a daemon (such as Podman). There are
also package managers that does a nice job for running server applications
(like Nix).

Second, I am probably going to get rid of useless subdomains. Sub-domains like
solarr.-- will be completely gone after this update. mc.-- is also useless so
this is also gonna be deleted.

Third, my file uploading service may change. uWSGI is garbage and absolutely
not required. AUS (The file uploading service) might be replaced by a simple
CGI script as I don't need more than that. uWSGI is a pain to configure and set
up and I don't want to struggle with it ever again.

There may have some wall layout changes, trying to make something that looks
better and eventually improving my static site generator to be more optimized
for blogging and for other stuff.

This layout was great, but I am someone who likes to change stuff often. The
layout change for now is not planned. It may come if I think it is really
worth it.

Finally, I want to add something I called Funbox. Funbox will be a small server
hosted on a Raspberry Pi where I will experiment stuff. I am announcing it here
because I may sometimes make my experiment publicly available through the
Funbox.

Get ready for a little downtime during this weekend! I am happy and proud to
contribute to this large network called the internet.
