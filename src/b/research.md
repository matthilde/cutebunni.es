# Starting a research lab

_Sat Jun  5 10:08:59 PM UTC 2021_

Sooo.... I want to start a research lab to learn more about programming languages
as I found some sort of interest into programming language development. Would be
a nice learning experience. I will also try to learn more about compilers and
VMs at the same time.

I will first start with a research programming language, a language that will be
easily hackable and easy to understand. Once an interpreter done, I will start
making other tools for the programming language, all for learning. MCUPL
(Matthilde's Completely Useless Programming Language) is a prototype of the
research programming language I wanted to create, it was initially supposed to
be written in Python as it would make hacking with the language easy but Python
is wacky when it comes to big codebases (and I also forgot to implement TCO :p).

There will probably have more research languages that will see the day light
probably, the purpose of this lab is just learning! I don't really seek any goal
but learning and fun.
