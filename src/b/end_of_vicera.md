# End of VICERA

_Fri Oct 30 01:44:53 PM UTC 2020_

## Introduction

The VICERA has been probably the biggest project I have ever worked on. This
was a really pleasing project to work on and is also the most starred GitHub
repo I ever had. Unfortunately I am stopping it's active development and
release the 1.0

This blog will talk about the final release of VICERA and why. This will mostly
cover the features included in the final release, the documentation, the
possible future of the VICERA and what have I learned from this project.

## Why? No more updates?

Well, I lost motivation and interest into this project and I think it is now
mature enough to make games on it and doesn't require any further development.  
The FIFO/Socket feature will remain unfinished unless someone PRs an
implementation for it. So is the native Windows support.

Also yes, there will probably have a few updates if I get to fix a few stuff on
it when I get bored or if anyone does a pull request. Pull Requests will still
be welcome and I won't also hesitate to read Issues. I will keep track on these
two things.

## What will appear in 1.0.0?

A new instruction set, an assembler, a debugger and a fresh new documentation
covering both the assembler and the VICERA.

I am currently still working on the documentation so you are not stuck with
reading my messy C code. There is also now a few debugging features that has
been implemented in the VICERA.

`fifo.c` and `fifo.h` will be kept in the repo but will never be added. There
was supposed to be replaced with a `socket.c`, which I never really worked on.
The `make fifo` command in the Makefile will also be deleted since the FIFO
feature has been cancelled.

The update should appear soon in the repo releases. Both for the VICERA and
the VICasm assembler.

## What have I learned...

I have learned how to plan a project, how to write down a TODO list to know
what to do next once I am done with a feature. I have learned that even if some
code is done, it can still be improved and fixed: The _If it works, don't touch
it_ philosophy is quite bullsh*t for long-term projects. I also learned how to 
write proper code and design functions.

This also helped me how to better understand memory allocation, pointers,
structs and type definitions in C which will probably help me a lot in some
future C projects.

This was an awesome experience to work on this project and I hope I will try
and make even more innovative work. Thanks you for reading my blog and have a
nice day!
