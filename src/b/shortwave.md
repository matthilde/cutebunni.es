# SWL, my new favourite hobby

_Mon Feb 28 09:18:17 AM CET 2022_

After getting lost in the internet for a while, I found out this
rather funny video of the UVB-76 radio station being jammed by pirates
to broadcast memes. I looked more into UVB-76 then learned about
shortwave radio, it is when I went into something that I am proud I
have stepped into: Shortware Listening and DXing. This was
overwhelming at first but I went over it rather quickly because of how
easy and exciting it actually is. It's a whole new hobby, opening
doors to me, some random highschool nerd. At the time of writing, I
have purchased my first receiver, an RTL-SDR, and I am waiting for it
to be delivered home. Thanks to online SDRs to get me into this hobby.

At first, I just decided to wonder randomly across the frequencies,
checking if there are interesting stuff to listen to. I could
sometimes find cool stuff -especially ham radio signals-. For some
reason, what caught my attention the most at first were ham radio
communications. This is when I actually started appreciating this
hobby. I remember having tried it months ago but felt overwhelmed and
gave up.

So as I said, what caught my attention at first was mostly ham radio
and utility signals for some reason. So I looked up how to decode them
and began setting up decoding software such as fldigi, QSSTV and WSJT-X.
QSSTV and WSJT are quite easy to use and worked out of the box. However,
fldigi took me some more time to get used to it and configure it, it
was also quite confusing at first. One useful resource I have used to
look for signals types at first was the
[Signal Identification Wiki](https://sigidwik.com), which I recommend.
Some of my first receptions are logged in [there](/shortwave/log1.html),
they are quite vague and unclear. Some identification is completely
wrong and I didn't bother recorrecting everything.
My favourite types of signals are FT8 for its simplicity and its
efficiency to use and SSTV because hehe funny images going through the
waves. This is after some more frequencies hunting that I spotted cool
stuff such as pirate radios.

Pirate Radios are the most interesting stations in my opinion, you do
not know when they'll pop, and unless you identify the radio, you
pretty don't know what content they're gonna give you. They're usually
sitting arround 6900kHz-7000kHz but there are also MW Pirates sitting
around 1600-1710kHz. There are more pirate freqs but these are the ones
that tends to be the most used by pirates as they are mostly unused by
other stations. The best stations I've listened to so far are WTF Radio
Worldwide, Mystery 21 and Radio Free Whatever.
[This log](/shortwave/log2.html) got plenty of recordings. I even got
an eQSL from WTF Radio Worldwide!

Pirate Radios are exciting and interesting when you catch them but
there are also licensed radios they are really nice! You can usually
find them on relays like Channel 292 or WRMI (when it's not
broadcasting cultist garbage) such as Radio Northern Europe
International, Pop Shop Radio or Shortwave Radiogram! SW Radiogram is
so far my favourite one as it does not broadcast music or voice, but
MFSK32 and MFSK64 signals that transmit news and images through text.
Really cool radio, there are some recordings and decodes of the
broadcasts [here](/shortwave/log2.html)!

This new hobby also kind of motivates me to get my Ham Radio license
for many reasons. First of all, communicating with the whole world with
it. I know there is internet but radio is cool too :). But I also
wanna experiment and try other stuff such as packet radio, there are
BBSes and other stuff like that over radio which seems really cool to
try out! And studying for the license is also a great opportunity to
learn how radio works and how to set up antennas, transmitters,
receivers, etc.

I already learned quite a lot from this experience so far (and I still
have a lot to learn!), I've learnd how shortwave radio propagate, how
it's emitted and received, I've also learned new kinds of communication
protocols outside, a new community that is really nice, accepting and
there to make you discover the world of SWL and Ham Radio! Really an
awesome experience. I still have other stuff to discover in the air
(even above shortwave radio!), like airport radio, utility stations or
spy number stations (these ones looks really interesting). I hope I
will have the opportunity to talk more about radio in other blogposts!