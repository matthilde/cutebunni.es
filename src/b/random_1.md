# Random Blog #1

_Thu Nov 19 10:16:38 PM UTC 2020_

Hello and welcome to this new series of blog where I will dump a bit the
current stuff happening. I know no one cares much but I just wanna write a blog
rn.

Recently I've been reading and learning a lot, I also have started developing a
kernel and looked into CPU architectures. Really interresting stuff

## Kernel development

So yeah, I have started working on a kernel (project is in hang tho :pensibe:).
This is a really cool experience, learning how the internals of a modern
operating system works. I am writing this kernel to toy arround with OSdev and
what I can do out of it.

The design of my kernel is simple, monolithic and monotasking. The purpose of
this project is to create a kernel to toy arround and experiment with.

I have also been looking into CPU stuff, since I have started developing the
VICERA, I have found CPUs really interresting to look into.

## Too many ideas

The reason why my current big project (the kernel) is in hang is because I am
having way too much ideas and I feel like it's lowering my motivation.
One would say that I am lucky to have so many ideas but it isn't, it most
likely makes me want to do everything at once and feel unmotivated doing one
project because I want to do another one. I think I should make a break, play
some Minecraft, read some articles on [Lobsters](https://lobste.rs)...

If you want I can give a small list of some ideas I am thinking about

 - VIC-16P CPU, a 16-bit extension of the VICERA CPU
 - DOS-like real mode operating system
 - Forth OS
 - Z80 micro-computer
 - Web server in Erlang
 - Re-organize all my server's filesystem
 - ...

I really hope I will get this overflow of ideas and get into one project.

## Why are you posting this?

Because blogging is not only a place where I publicly talk about my projects
and such. It makes me happy to write blogs and this is the ultimate reason why
I maintain a blog. I don't think about having a large audience, just a place
where I can write stuff.

Once my break will be done and I continue working on my kernel, I will write a
blog about it :)

Anyways, have a good day and stay safe!
