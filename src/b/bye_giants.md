# Fleeing tech giants and mainstream internet/software...

_Mon Mar 15 11:06:16 AM CET 2021_

## Introduction

2 years ago, I have noticed a degradation of proprietary software. Software got
more complex, heavier, but also more unethical and unstable. What made me truly
switch from Windows to Linux is how Windows 10 has been a terrible experience to
me. It took me 5-6 months to finally completely switch to Linux. Backed up all
my data, formatted my hard drive and installed Arch Linux over it.

By installing Arch Linux, I realised Open Source Software was much more stable
and reliable than some of the proprietary software I was using before as Linux
distro repos has in majority (free and) open source software. At this point I
started migrating from what I was mostly using to FOSS. But recently I have been
doing a (almost) complete migration from all these services and software.

Tech giants tends to make bad and unethical software and services. Resulting of
usually slow software, heavy webpages, tracking you down for the sake of ads.
I don't want that, I don't want my personal data to be saved in a server for ads
and recommendations. They don't respect our privacy nowadays anymore and there
is sorta "money over user" philosophy that is just not right to me. This is
why I gave up on their stuff.

So far I almost got rid of everything except a few things. My text editor is
FOSS, my browser is FOSS, almost no trace of tech giants anymore. I am keeping
Discord and WhatsApp for my friends though.

## Switching software

Switching from one software to another wasn't very easy to me, nor too difficult
though. Some software was harder to switch from like VSCode or even Windows
itself but I did it!

The first thing I have switched from and was also probably the hardest part
overall was to switch from Windows to Linux. I was used of Windows' 100%
graphical interface without much command-line, it's user-friendliness but most
importantly their wide selection of software. A lot of the software I was using
was only compatible on Windows and some of them weren't even possible to run
easily on something like Wine.

To get over this difficulty, I have sorted all the installed software and think
"What is actually useful, what do I actually use and what can I get rid of?",
asking yourself this kind of question can ease the problem of "but my software
won't run on Linux". Look at what you actually need.

My other issue when switching was that uh... I didn't picked the easiest path.
I had experience with Ubuntu before but not enough to go directly into an
advanced Linux distribution, but I still choosed to switch to Arch Linux. A good
thing about this distribution is that their wiki is very complete and documents
more than just their distribution and the software bundled in. It also documents
3rd party software. So I managed to install it and use it without a lot of
problems.

Another software I switched from was Visual Studio Code, VSCode is proprietary
and made by Microsoft, which is not good. I first switched to VSCodium, which
are OSS binaries of VSCode. It gets rid of all the Microsoft telemetry and
proprietary stuff. A few months later, I switched to neovim because I found it
to be faster and a better programming experience for me.

### Benefits from using Linux/BSD

When I switched to Linux, I had to get used of the way it worked: Linux has a
completely different way of functionning from Windows. Even if the community
makes GUIs to ease configuration and general experience at a point one can just
download a distro which has GUI for everything, I decided to get into the CLI
experience and give it a try. There had no regret in my choice, now I do a lot
of stuff directly in my terminal emulator. Not because GUI software is Bloat(TM)
but just because I find the CLI comfy.

Another benefits I had for using Linux was that I know what the hell is
happening whenever I fire up `htop` in my terminal. There is no process that I
have absolutely no idea why it exists that is using 100% of the CPU usage and
make my fans spin.

It's also easy to automate tasks (mostly for CLI folks). Shell scripts and other
scripting languages are your friends, unlike the clunky PowerShell and Batch
duo.

I'd like to comment out that if you don't like Linux, it's completely fine and
I know many users prefer using Windows for many reasons. I see a lot of people
trying to convert people (that is not obviously interested in technology) to
Linux which is just stupid to me.

## Social medias

This was one of the easiest part of my migration as I wasn't really active on
any other social media than Discord (which I kept for the only reason that I
have friends who wouldn't be willing to switch to something else, which is
completely valid and fair). Bye Reddit, Twitter (who suspended me and I still
have no fucking idea why), Instagram, whatever. Hello Fediverse, self-hosted
blogs and ActivityPub :)

Independent and handwritten sites are probably the best website content I have
ever seen. They have a much higher content quality than other stuff. People take
time to write HTML by hand, write interesting blogs, share their projects and art.
And they are usually much more lightweight than what we usually see in the
modern web.

I've never been a fan of social networks before. Using Discord for the sole
purpose of it being a communication tool. Twitter and Reddit was boring to me.
For some reason Mastodon was really cool and I loved it at the first minute I
gave it a try.

Another "social media" I like are pubnixes, these are public access UNIX
computers shared by a few people. These usually provide programming tools, text
editors, HTTP/Gemini/Gopher servers, IRC, games, etc. It's really good and fun
to chill in.

## Storing my code somewhere else...

GitHub is bought by Microsoft. And Microsoft is known to fuck up everything they
buy (examples: Nokia, Minecraft and Skype).

I moved to a self-hosted Gitea instance and Codeberg. I think they are much
better. I now go on GitHub or sourcehut when I wanna explore cool projects. I
don't store my code in there anymore.

## Conclusion

Trying to flee away tech giants actually made my computer experience better.
With computers that lasts longer, better reliability and stability, and also
fleeing tech giants made me discover a really cool people in the low-end
computing and FOSS community (techbros and fossbros excluded lol).

It wasn't easy to do this transition but I am slowly doing it. Privacy and better
software is the reward of this effort.

I am not trying to tell anyone to do this transition like I did. This is a
write-up of this really interesting experience. If you like what you're using,
then it's completely fine. You do you :)

It's been a while I haven't blogged,, feels good uwu
