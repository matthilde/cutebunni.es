# Creating unix.lgbt

*Thu Apr 15 09:50:58 PM CEST 2021*

## Introduction

unix.lgbt is a pubnix (public access UNIX system). A pubnix is a social space
where people can come here to create stuff, talk with other people and learn how
to program and use a UNIX system. We hope becoming part of the tildeverse at
some point to expand our community!

The idea of creating this pubnix came from a Discord server named "be gay do
computer" (previous named "LGBTQ+/*NIX"). We thought it would be fun, cool and
good to make this. And we were right! It's really fun to work on and also an
amazing experience, for both the users and us, admins of this box.

Btw, **this blog is not a tutorial about how to create a pubnix but a write-up
of my experience in the creation process of unix.lgbt. You may use some elements
from this post to create your own though!**

## Setting up the box

The first thing we obviously had to do is getting a Linux server. It was
initially going to be an old 2006 machine laying in my bedroom but turns out
someone got a dedi to run it on.
[You can see the specs in this neofetch dump](https://microwave.goose.business/neofetch.txt).

Now we need an UNIX-like operating system. I initially thought about using the
[KISS Linux](https://k1sslinux.org/) distribution but it would be too painful
to maintain and set up so we have choosen Alpine Linux instead. It is a
lightweight Linux distribution running on Busybox, musl libc and OpenRC.

~~I kinda regret using OpenRC because it is kinda pain. No logs :v~~

## Services

We obviously can't just give just a dumb SSH access to an fresh Linux install,
we have to install software and set up some services to make it more exciting
and interesting! We have decided to host a webserver with per-user website, a
Gemini server, an IRC server so we can talk together, a mail server for
registration confirmations, our very own BBS and a user-maintained package
manager.

### Webserver

I have used my webserver of choice for this one which is NGINX. Setting
everything up was quite simple using regex matches like this:

    server {
        server_name unix.lgbt;
        listen 443 ssl;
        # ... ssl stuff ...
        
        location ~ ^/~(?<user>[^/]+?)(?<user_uri>/.*)?$ {
            alias /pub/$user/http/$user_uri;
            error 404 /~$user/404.html  # custom 404 hehe
            autoindex on;
            
            # ... fcgiwrap stuff ...
        }
    }
    
We (well mostly I) also have decided to add domain names dedicated to user
websites. Here are the amazing domain names we have

 - goose.business
 - infinitely.gay
 - be-gay-do.computer (donated by [~ari](https://ari.be-gay-do.computer))

Configurating the subdomains was even simpler than the previous configuration.
We will use goose.business as the example domain.

    server {
        server_name ~^(?<user>.+)\.goose\.business;
        listen 443 ssl;
        # ... ssl stuff ...
        
        index index.html index.cgi ... ;
        root /pub/$user/http;
        error_page /404.html;
        autoindex on;
        
        # ... fcgiwrap stuff ...
    }
    
### IRC and mail

it was pain. all you need to know is that i used `ngircd` for IRC and `maddy`
for mail. mail was the most painful.

### forum protocol and package manager

forum is our very own BBS protocol. We have designed it to make it easy to work
with. I will talk more about it later.

we also have our own local package manager maintained by the community.

## Registration process

To set up registration requests, we have decided to do everything from scratch.
From the form to the automated mail confirmation script. As PHP is decent for
quick and dirty scripts, I have decided to write the form in PHP (also because
it supports Securimage, which is a JavaScript-less open source Captcha system).

Once the registration sent, the PHP script will run a Python script (yes it is
messy but did you *really* expected I would use PHP the whole time?). The script
will take the email, the SSH key, the username and the deal in a file under the
name of an unique ID and will send a webhook on our Discord server to review
the request.

If we decline, we just have to delete the request file. If we accept, we just
have to run an `acceptuser` script that will add the user into the pubnix, set
up the directories and send them an email with a randomly generated password.

When an user connects into the box, they will be welcome with a first-run
script where they can choose their shell, their userland and their new password.

We also have a `revokeuser` script in case we need to get rid of a user.

## The forum protocol

The forum protocol is our own BBS as specified before in the blog. We have
decided to make something simple that relies in the filesystem so it is easy
to parse and to implement clients. There are excellent forum clients around such
as [makefile's http client](https://makefile_dot_in.is.infinitely.gay/forum/).

You can learn more about the protocol [here](https://microwave.goose.business/manpages/?page=forum).

## User-maintained package manager

To easily share our programs and creations, we have decided to create a
user-maintained package manager that is, again, using the filesystem as a
database.

[Documentation](https://microwave.goose.business/manpages/?page=pm)

Packages are contained in `/pub/USERNAME/pkg`. And each package is a folder
containing an executable of the same name as the package name and a one-line
description.

Packages can be directly executed using `pm e pkgname` or installed at `~/bin`
using `pm i pkgname`. Every package is just one executable, you can for example
script an installer if you require more than that one executable to be installed.

But since the issues of dependencies tends to be solved due to the shared machine,
we do not need to build anything or download missing deps.

## /pub directory

The pub directory is somehow the public user resources such as their public http
directory or just public files. It is also where forum posts and user packages
are stored. We designed this as an alternative to leaving our `/home` dir public
and making the creation of "services" easy (see
[shitter by ~eris](https://hammy.goose.business/source/?/pub/eris/pkg/shitter/shitter))

## Conclusion

Thanks you all for making this project alive, thanks to all of you for reaching
the 50 users milestone, something that was just unexpected. I love to see this
community grow everyday and see the stuff people makes.

Here is my list of cool projects I have tested here :
https://microwave.goose.business

I plan to continue maintaining this box, having a such heartwarming community
does nothing but motivating me into keeping maintaining the box.
