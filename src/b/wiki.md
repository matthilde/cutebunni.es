# Creating a Wiki

_Wed Jan 27 07:58:54 AM CET 2021_

Hello! Been a while I haven't posted anything. I have decided to create a wiki
on my website which will appear [here](https://wiki.matthil.de).

The VICERA documentation will probably be rewritten and added here.
The wiki will basically just be a place where I note everything and nothing. You
may find interesting info about some software, ideas and personal projects.

It will be made with vimwiki!

That was a really small blog but hopefully I'll finish my blog in progress. It
is quite big and things has changed since I started working on it. So I might
have to do changes.

Stay tuned, it may come... eventually.

**UPDATE** It's dead now but you can still visit it.