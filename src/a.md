# About me

I am matthilde, some random high-schooler who is passionate about
computer science, radio and just technology in general. I decided to
make this site so I could share about my hobby.

What you can find in this site are mostly C and Python projects but
also some reception logs done from my radio equipment.

I also have some interest in LGBTQ activism, sociology and
politics (especially socialism). But I don't feel ready about posting
anything related to those topics as I don't feel like my knowledge
about those subjects is developed enough.

I go by she/her.

 * **GitHub:** [matthilde](https://github.com/matthilde)
 * **Codeberg:** [matthilde](https://codeberg.org/matthilde)
 * **Contact me:** `printf 'bWF0dGhpbGRlQHBtLm1l' | base64 -d -`
